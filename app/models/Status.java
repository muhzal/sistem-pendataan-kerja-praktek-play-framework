package models;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import com.google.gson.annotations.Expose;

import play.db.jpa.Model;
@Entity
public class Status extends Model {

	public String namastatus;
	@OneToMany(mappedBy="status",cascade=CascadeType.ALL)
	public Collection<Siswa> siswa;
}
