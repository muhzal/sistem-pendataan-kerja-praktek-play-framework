package models;

import java.util.Collection;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.Max;

import play.data.validation.MaxSize;
import play.data.validation.Unique;
import play.db.jpa.Model;
@Entity
public class Pengajuan extends Model {
	@Unique
	public String nosurat;
	public Date tglsurat;
	@ManyToOne(cascade=CascadeType.ALL)
	public JenisInstansi jenisinstansi_id;
	@Column(length=50)
	public String namainstansi;
	@Column(length=15)
	public String notelp;
	public int jumlahorang;

	@OneToMany(cascade=CascadeType.ALL,mappedBy="pengajuan")
	public Collection<Siswa> siswa;
	public boolean tampil;
	
}
