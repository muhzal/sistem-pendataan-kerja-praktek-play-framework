package controllers;

import java.util.List;

import models.Divisi;
import models.JenisKelamin;
import models.Level;
import models.Siswa;
import models.User;
import play.mvc.Controller;
import play.mvc.With;
@With(Secure.class)
@Check("admin")
public class Users extends Controller {

	public static void hapususer(long id) throws Throwable{
		if (Home.getNama().id == id ) {
	        session.clear();
			User user=User.findById(id);
			user.delete();
			//Secure.login();
			renderText("logout");
		}else{			
			User user=User.findById(id);
			user.delete();
			index();
		}
	}
	public static void index(){
		List<JenisKelamin> jk=JenisKelamin.findAll();
		List<Level> level=Level.findAll();
		render(jk,level);
	}
	public static void listuser(){
		List<User> user=User.findAll();
		render(user);
	}
	public static void tambahuser(User user){
		if (user.id==null) {
			if(User.find("nip=?", user.nip).fetch().size()>0 && User.find("username=?", user.username).fetch().size()>0){
				renderText("nip+username");
			}else if(User.find("nip=?", user.nip).fetch().size()>0){
				renderText("nip");
			}else if(User.find("username=?", user.username).fetch().size()>0){
				renderText("username");
			}
			try {
					user.save();
					renderText("success");
				} catch (Exception e) {
					renderText(e.toString());
				}
		}else{

			User lama=User.find("id=?",user.id).first();
			User nipbaru=User.find("nip=?",user.nip).first();
			User usernamebaru=User.find("username=?",user.username).first();
			if(User.find("username=?", user.username).fetch().size() >= 1 && usernamebaru.username != lama.username && User.find("nip=?", user.nip).fetch().size() >= 1 && nipbaru.nip != lama.nip){
				renderText("nip+username");
			}else if(User.find("nip=?", user.nip).fetch().size() >= 1 && nipbaru.nip != lama.nip){
				renderText("nip");
			}else if(User.find("username=?", user.username).fetch().size() >= 1 && usernamebaru.username != lama.username){
				renderText("username");
			}
			try {
				user.save();
				renderText("success");
			} catch (Exception e) {
				renderText(e.toString());
			}
		}
	}

	public static void getNameBeforeDelete(long id){
		User user=User.findById(id);
		renderText(user.username);
	}
	public static void ubahuser(long id){
		User user=User.findById(id);
		List<JenisKelamin> jk=JenisKelamin.findAll();
		List<Level> level=Level.findAll();
		render(user,jk,level);
	}


}
