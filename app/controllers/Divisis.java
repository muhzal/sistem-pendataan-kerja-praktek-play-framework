package controllers;

import java.util.List;

import models.Divisi;
import models.Siswa;
import models.Status;
import play.mvc.Controller;
import play.mvc.With;
@With(Secure.class)
@Check("admin")
public class Divisis extends Controller {

	public static void cekkuota(long id){
		Divisi divisi=Divisi.findById(id);
		renderText(divisi.kuota);
	}
	public static void detail(long id){
		Divisi divisi=Divisi.findById(id);			
		render(divisi);
	}
	public static void detaildivisi(long id){
		Divisi divisi=Divisi.findById(id);
		int proses=Siswa.find("pengajuan.tampil=true and divisi.id=? and status.id=1", id).fetch().size();
		int disetujui=Siswa.find("pengajuan.tampil=true and divisi.id=? and status.id=3", id).fetch().size();
		int menunggu=Siswa.find("pengajuan.tampil=true and divisi.id=? and status.id=4", id).fetch().size();	
		int selesai=Siswa.find("pengajuan.tampil=true and divisi.id=? and status.id=6", id).fetch().size();		
		render(divisi,proses,menunggu,disetujui,selesai);
	}
	public static int hitungSiswa(long id){
		int proses=Siswa.find("pengajuan.tampil=true and divisi.id=? and status.id=1", id).fetch().size();
		int disetujui=Siswa.find("pengajuan.tampil=true and divisi.id=? and status.id=3", id).fetch().size();
		int menunggu=Siswa.find("pengajuan.tampil=true and divisi.id=? and status.id=4", id).fetch().size();	
		int selesai=Siswa.find("pengajuan.tampil=true and divisi.id=? and status.id=6", id).fetch().size();		
		return menunggu+proses+disetujui+selesai;
	}
	public static void index(){
		render();
	}
	public static void listdivisi(){
		List<Divisi> divisi=Divisi.find("tampil=true").fetch();						
		render(divisi);
	}
	public static void listsiswa(long id){
		List<Siswa> siswa=Siswa.find("divisi.id=? and status.id!=6 and status.id!=7 and status.id!=2 order by status.id",id).fetch();
		List<Status> status=Status.find("id!=7").fetch();
		render(siswa,status);
	}
	public static void tambahdivisi(Divisi divisi){
		try {
			divisi.tampil=true;
			divisi.save();
			renderText("success");
		} catch (Exception e) {
			e.printStackTrace();
			renderText(e.toString());
		}
	}	
	public static void ubahdivisi(long id){
		Divisi divisi=Divisi.findById(id);
		render(divisi);
	}
	public static boolean cekdivisi(long id){		
		Divisi divisi=Divisi.findById(id);
		if (divisi.terisi+hitungSiswa(id)>0) {			
			return false;
		}else{
			return true;
		}
		
	}
	public static void hapusdivisi(long id){
		try {
			Divisi divisi=Divisi.findById(id);
			if (divisi.terisi==0) {
				divisi.tampil=false;
				divisi.save();
				renderText("sukses");
			}else{
				renderText("terisi");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}
