package controllers;
 
import java.util.List;

import controllers.Secure.Security;
import models.Divisi;
import models.JenisKelamin;
import models.Level;
import models.Pengajuan;
import models.Siswa;
import models.User;
import play.mvc.Controller;
import play.mvc.With;
@With(Secure.class)
public class Home extends Controller {
	
	public static void index(){
		int jmlpengajuan=Pengajuan.find("tampil=true").fetch().size();
		int jmlsmk=Siswa.find("pengajuan.jenisinstansi_id.id=1 and status.id=5").fetch().size();
		int jmlmahasiswa=Siswa.find("pengajuan.jenisinstansi_id.id=2 and status.id=5").fetch().size();
		int jmlmenunggu=Siswa.find("status.id=4").fetch().size();
		List<Siswa> menunggu=Siswa.find("status.id=4 order by tglmulai ASC").fetch(10);
		List pengajuan=Pengajuan.find("order by tglsurat DESC").fetch(5);
		List<Divisi> divisi=Divisi.find("tampil=1").fetch();
		render(divisi,pengajuan,jmlpengajuan,jmlsmk,jmlmahasiswa,jmlmenunggu,menunggu);
	}
	
	public static User getNama(){
		User user=User.find("username", Security.connected()).first();
		return user;
	}

	public static void tentang(){
		render();
	}

	public static void ubahpassword(String passwordsekarang,String passwordbaru1,String passwordbaru2){
		if (!Home.getNama().password.equals(passwordsekarang)) {
			renderText("salah");
		}else{
			Home.getNama().password=passwordbaru1;
			Home.getNama().save();			
			renderText("sukses");
		}
	}
}
