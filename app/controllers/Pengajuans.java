package controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import models.Divisi;
import models.JenisInstansi;
import models.JenisKelamin;
import models.Pengajuan;
import models.Siswa;
import models.Status;
import play.data.binding.As;
import play.data.validation.IsTrue;
import play.mvc.Controller;
import play.mvc.With;

@With(Secure.class)
@Check("admin;SDM")
public class Pengajuans extends Controller {

	public static void arsip(){
		render();
	}	
	public static void arsipkanpengajuan(long id){		
		try {
			Pengajuan pengajuan=Pengajuan.findById(id);
			pengajuan.tampil=false;
			pengajuan.save();
			renderText("sukses");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			renderText(e.toString());
		}
	}	
	public static void restorepengajuan(long id){		
		try {
			Pengajuan pengajuan=Pengajuan.findById(id);
			pengajuan.tampil=true;
			pengajuan.save();
			renderText("sukses");
		} catch (Exception e) {
			// TODO: handle exception
			
			renderText(e.toString());
		}
	}	
	public static void detail(long id){
		Pengajuan pengajuan=Pengajuan.findById(id);
		List<Status> status=Status.find("id!=7").fetch();
		List<Divisi> divisi=Divisi.find("tampil=1").fetch();
		List<JenisKelamin> jk=JenisKelamin.findAll();
		if (pengajuan==null) {
			notFound();
		}
		render(pengajuan,status,divisi,jk);
	}
	public static void detailpengajuan(long id){
		Pengajuan pengajuan=Pengajuan.findById(id);
		render(pengajuan);
	}	
	public static void formsiswa(int banyak){
		List<Divisi> divisi=Divisi.find("tampil=1").fetch();
		List<JenisKelamin> jk=JenisKelamin.findAll();
		List<Status> status=Status.find("id!=7").fetch();
		render(jk,banyak,divisi,status);
	}
	public static void getNamePengajuan(long id){
		Pengajuan pengajuan=Pengajuan.findById(id);
		renderText(pengajuan.namainstansi);
	}
	public static void index(){
		List<JenisInstansi> jenis=JenisInstansi.findAll();
		render(jenis);
	}
	public static void listPengajuan(int display){
		List pengajuan=null;
		if (display==1) {
			pengajuan=Pengajuan.find("tampil=? order by tglsurat asc",true).fetch();
			render(pengajuan);
		}else if(display==0){
			pengajuan=Pengajuan.find("tampil=? order by tglsurat asc",false).fetch();
			render(pengajuan);
		}else{
			notFound();
		}
	}
	public static void simpanpengajuan(Pengajuan pengajuan,@As("dd-MM-yyyy") Date tglsurat,List<Siswa> object,@As("dd-MM-yyyy") Date[] tglmulai,@As("dd-MM-yyyy") Date[] tglselesai){	
		int index=0;
		if (pengajuan.id != null) {
			try {
				pengajuan.tglsurat=tglsurat;
				pengajuan.save();
				renderText("success");
			} catch (Exception e) {
				// TODO: handle exception
				renderText(e.toString());
			}
		}else{
			try {
				pengajuan.tglsurat=tglsurat;
				pengajuan.tampil=true;
				pengajuan.save();
				Status status=Status.find("id=1").first();
				for (Siswa siswa : object) {	
					siswa.status=status;	
					siswa.pengajuan=pengajuan;
					siswa.tglmulai=tglmulai[index];
					siswa.tglselesai=tglselesai[index];
					siswa.save();
					index++;
				}
				renderText("success");
			} catch (Exception e) {
				e.printStackTrace();
				renderText(e.toString());
			}
			}
	}
	public static boolean statusArsipkan(long id){
		boolean hasil=true;
		int counter=0;
		Pengajuan pengajuan=Pengajuan.findById(id);
		for (Siswa siswa : pengajuan.siswa) {
			if (siswa.status.id==6 || siswa.status.id==2) {
				counter++;
			}
		}
		if (counter==pengajuan.siswa.size()) {
			hasil=false;
		}
		return hasil;
	}
	public static void ubahpengajuan(long id){
		Pengajuan pengajuan=Pengajuan.findById(id);
		List<JenisInstansi> jenis=JenisInstansi.findAll();
		render(pengajuan,jenis);
	}


	
}










