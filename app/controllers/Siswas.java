package controllers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import models.Divisi;
import models.JenisKelamin;
import models.Pengajuan;
import models.Siswa;
import models.Status;
import play.data.binding.As;
import play.db.jpa.JPA;
import play.mvc.Controller;
import play.mvc.With;
@With(Secure.class)
@Check("admin;SDM")
public class Siswas extends Controller {	

	public static void arsip(int menu){
		if (menu == 5 || menu == 6 || menu==7) {
			render(menu);
		}else{
			notFound();
		}
	}	
	public static void cekkuotadivisi(long id){
		Divisi divisi=Divisi.findById(id);
		renderText(divisi.kuota);
	}
	public static boolean ceksertifikat(long id){
		boolean hasil=false;
		Siswa siswa=Siswa.findById(id);
		if (siswa.status.id==6 || siswa.status.id==7) {
			hasil=true;
		}
		
		return hasil;
	}
	public static void getNameBeforeDelete(long id){
		Siswa siswa=Siswa.findById(id);
		renderText(siswa.nama);
	}
	public static void hapus(long id){
		try {

			Siswa siswa=Siswa.findById(id);
			siswa.delete();
			renderText("success");
		} catch (Exception e) {
			// TODO: handle exception
			renderText(e.toString());
		}
	}
	public static void listsiswa(int menu,long pengajuan){
		List<Siswa> siswa=null;
		List<Status> status=Status.find("id!=7").fetch();
		switch (menu){
		case 1:
			siswa=Siswa.find("pengajuan.tampil=true and pengajuan.jenisinstansi_id=1 and status.id!=2 order by id ASC ").fetch();
			pengajuan=0;
			break;
		case 2:
			siswa=Siswa.find("pengajuan.tampil=true and pengajuan.jenisinstansi_id=2 and status.id!=2   order by id ASC ").fetch();
			pengajuan=0;
			break;	
		case 3:
			siswa=Siswa.find("pengajuan.id=?",pengajuan).fetch();
			break;	
		case 4:
			siswa=Siswa.find("pengajuan.tampil=true and divisi.id=? and status.id!=2",pengajuan).fetch();
			break;			
		case 5:
			siswa=Siswa.find("pengajuan.tampil=false and pengajuan.jenisinstansi_id=1 and status.id!=2 order by id ASC ").fetch();
			pengajuan=0;
			break;	
		case 6:
			siswa=Siswa.find("pengajuan.tampil=false and pengajuan.jenisinstansi_id=2 and status.id!=2 order by id ASC ").fetch();
			pengajuan=0;
			break;	
		case 7:
			siswa=Siswa.find("pengajuan.tampil=false and status.id=2 order by id ASC ").fetch();
			pengajuan=0;
			break;
		case 8:
			siswa=Siswa.find("pengajuan.tampil=true and status.id=4 order by id ASC ").fetch();
			pengajuan=0;
			break;	
		default:
			notFound();
			break;
		}
		render(siswa,status,menu,pengajuan);
	}
	public static void index(int menu){
		if (menu == 1 || menu == 2) {
			render(menu);
		}else{
			notFound();
		}
	}	
	public static void simpanstatussiswa(Status status,long id){
		try {		
			Siswa siswa=Siswa.findById(id);
			if (!siswa.status.equals(status)) {
				Divisi divisi=Divisi.findById(siswa.divisi.id);
				if (divisi.kuota==0 && status.id==5) {					
					renderText("full");
				}else{
					siswa.status=status;				
					siswa.save();	
					renderText("success");
				}
			}else{
				renderText("tetap");
			}
		} catch (Exception e) {
			renderText(e.toString());
		}
	}
	public static void tambahsiswa(long idpengajuan){
		List<Status> status=Status.find("id!=7").fetch();
		List<Divisi> divisi=Divisi.find("tampil=1").fetch();
		List<JenisKelamin> jk=JenisKelamin.findAll();
		render(idpengajuan,status,divisi,jk);
	}
	public static void ubahsiswa(long id){
		List<Status> status=Status.find("id!=7").fetch();
		List<Divisi> divisi=Divisi.find("tampil=1").fetch();
		Siswa siswa=Siswa.findById(id);
		List<JenisKelamin> jk=JenisKelamin.findAll();
		render(siswa,divisi,status,jk);
	}
	public static void simpansiswa(Siswa siswa,@As("dd-MM-yyyy") Date tglmulai,@As("dd-MM-yyyy") Date tglselesai){
		try {		
			siswa.tglmulai=tglmulai;
			siswa.tglselesai=tglselesai;	
			siswa.save();			
			renderText("success");
		} catch (Exception e) {
			renderText(e.toString());
		}
	}
}

