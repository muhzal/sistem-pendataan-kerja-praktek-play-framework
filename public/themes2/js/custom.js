$(document).ready(function () {
	/*====================================
    METIS MENU 
    ======================================*/
    $('#main-menu').metisMenu({
    	toggle: false
    });
    

	jQuery.extend(jQuery.validator.messages, {
	    required: "Bagian ini harus diisi.",
	    remote: "Please fix this field.",
	    email: "Please enter a valid email address.",
	    url: "Please enter a valid URL.",
	    date: "Please enter a valid date.",
	    dateISO: "Please enter a valid date (ISO).",
	    number: "Bagian ini harus diisi menggunakan nomor.",
	    digits: "Please enter only digits.",
	    creditcard: "Please enter a valid credit card number.",
	    equalTo: "Password yang anda masukan tidak cocok",
	    accept: "Please enter a value with a valid extension.",
	    maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
	    minlength: jQuery.validator.format("Please enter at least {0} characters."),
	    rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
	    range: jQuery.validator.format("Please enter a value between {0} and {1}."),
	    max: jQuery.validator.format("Jumlah orang tidak boleh melebihi {0} orang."),
	    min: jQuery.validator.format("Jumlah orang tidak boleh kurang dari {0} orang.")
	});
    var validator=$('#form-ubah-password').validate({
    	errorClass: "field-error",
    	rules:{
    	    passwordbaru1: "required",
    	    passwordbaru2: {
    	      equalTo: "#passwordbaru1"
    	    }
    	}
    });

	$('#modal-ganti-password').on('hide.bs.modal',function(event){			
			validator.resetForm();
	});	
	$('#modal-ganti-password').on('hidden.bs.modal',function(event){			
		validator.resetForm();
});	
    /*====================================
    	Active Menu
    ======================================*/
    	var url = window.location;
        $('#main-menu li a[href="'+ url +'"]').addClass('active-menu');
        // Will also work for relative and absolute hrefs
	     $('#main-menu li a').filter(function() {
	         return this.href == url;
	     }).addClass('active-menu');
	     
         /*====================================
           LOAD APPROPRIATE MENU BAR
        ======================================*/
         $(window).bind("load resize", function () {
             if ($(this).width() < 768) {
                 $('div.sidebar-collapse').addClass('collapse')
             } else {
                 $('div.sidebar-collapse').removeClass('collapse')
             }
         });    


    });


